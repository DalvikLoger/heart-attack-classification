#Hotel
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() # pour modifier le thème

import re
df = pd.read_csv('heart.csv.xls',header=0 )
df.head()

#Arbre de décision
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

data = df.drop(['output'], axis=1)
target = df['output']

X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.2, random_state=123)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

#SVM
from sklearn.svm import SVC
svm = SVC(gamma='scale')
svm.fit(X_train, y_train)

print('Score sur ensemble test', svm.score(X_test, y_test))

y_pred = svm.predict(X_test)

print(pd.crosstab(y_test, y_pred, colnames=['Predictions']))