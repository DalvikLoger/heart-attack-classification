#Hotel
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() # pour modifier le thème

import re
df = pd.read_csv('heart.csv.xls',header=0 )
df.head()

#Arbre de décision
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

data = df.drop(['output'], axis=1)
target = df['output']

X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.2, random_state=123)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

from tensorflow.keras.layers import Input, Dense #Pour instancier une couche Dense et une d'Input
from tensorflow.keras.models import Model

from sklearn.preprocessing import LabelEncoder
encoder =  LabelEncoder()
Y = encoder.fit_transform(target)

from sklearn.model_selection import train_test_split
X_train,X_test, y_train,y_test = train_test_split(data,Y,test_size=0.33,random_state=42) 

inputs = Input(shape = (13), name = "Input")

dense1 = Dense(units = 512, activation = "tanh", name = "Dense_1")
dense2 = Dense(units = 256, activation = "tanh", name = "Dense_2")
dense3 = Dense(units = 128, activation = "tanh", name = "Dense_3")
dense4 = Dense(units = 64, activation = "tanh", name = "Dense_4")
dense5 = Dense(units = 32, activation = "tanh", name = "Dense_5")
dense6 = Dense(units = 8, activation = "tanh", name = "Dense_6")
dense7 = Dense(units = 4, activation = "tanh", name = "Dense_7")
dense8 = Dense(units = 2, activation = "softmax", name = "Dense_8")

x=dense1(inputs)
x=dense2(x)
x=dense3(x)
x=dense4(x)
x=dense5(x)
x=dense6(x)
x=dense7(x)
outputs=dense8(x)

model = Model(inputs = inputs, outputs = outputs)
model.summary()

model.compile(loss = "sparse_categorical_crossentropy",
              optimizer = "adam",
              metrics = ["accuracy"])

model.fit(X_train,y_train,epochs=150,batch_size=32,validation_split=0.1)

y_pred = model.predict(X_test)

y_test_class = y_test
y_pred_class = np.argmax(y_pred,axis=1)
from sklearn.metrics import classification_report,confusion_matrix
print(classification_report(y_test_class,y_pred_class))
print(confusion_matrix(y_test_class,y_pred_class))